package com.spring.producer.Kafka;

import com.cleartrip.analytics.AirDetails;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;

public class randomMessage {
    public static AirDetails.airMessage randomMessageGenerator(){
        AirDetails.airMessage.Builder message = AirDetails.airMessage.newBuilder();
        AirDetails.airDetails.Builder air = AirDetails.airDetails.newBuilder();
        air.setSearchQuery(RandomStringUtils.randomAlphabetic(2));
        air.setAirLineCarrier(RandomStringUtils.randomAlphabetic(3));
        List<AirDetails.userDetails> userDetailsList = new ArrayList<>();
        AirDetails.userDetails.Builder userDetails = AirDetails.userDetails.newBuilder();
        userDetails.setUserId(Integer.parseInt(RandomStringUtils.randomNumeric(5)));
        userDetails.setFullName(RandomStringUtils.randomAlphabetic(10));
        userDetails.setGender(RandomStringUtils.randomAlphabetic(1));
        userDetails.setAge(Integer.parseInt(RandomStringUtils.randomNumeric(2)));
        userDetails.setPassengerType(AirDetails.passengerType.valueOf(1));
        userDetailsList.add(userDetails.build());
        message.setAirDetail(air);
        message.addAllUserDetail(userDetailsList);
        message.setSource(RandomStringUtils.randomAlphabetic(10));
        message.setDestination(RandomStringUtils.randomAlphabetic(10));
        AirDetails.airMessage msg = message.build();
        return msg;
    }
}
