package com.spring.producer.Kafka;
import com.cleartrip.analytics.AirDetails;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Properties;
@Component
public class kafkaMessage{
    @Scheduled(fixedRate = 1000)
    public static void sendKafkaMessage(){
        Properties producerProperties = new Properties();
        String kafkaBootstrapServers = "localhost:9092";
        producerProperties.put("bootstrap.servers",kafkaBootstrapServers);
        producerProperties.put("acks", "all");
        producerProperties.put("retries", 0);
        producerProperties.put("batch.size", 16384);
        producerProperties.put("linger.ms", 1);
        producerProperties.put("buffer.memory", 33554432);
        producerProperties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        producerProperties.put("value.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer");
        KafkaProducer<String, byte[]> producer = new KafkaProducer<>(producerProperties);
        AirDetails.airMessage payload = randomMessage.randomMessageGenerator();
        String topic = "air-messages";
        producer.send(new ProducerRecord<>(topic, payload.toByteArray()));
    }
}
