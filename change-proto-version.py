import os
import re
import sys

def getProtoUsingRepoList():
    # Gets the repo that uses protofiles.
    lst = get_gradlebuild_list(".")
    repoList = list()
    for filePath in lst:
        with open(filePath, 'r') as file :
            filedata = file.read()

        pattern = re.compile("protofiles")
        dependency_list = re.findall(pattern,str(filedata))
        if len(dependency_list) != 0:
            #print(dependency_list)
            repoList.append(filePath)
    print("Build-gradle-list: ")
    for i in repoList:
        print(i)
    print("End")
    repoList = list(set(repoList))
    repoList = list(set([repo.split('/')[1] for repo in repoList]))
    
    #if len(repoList) == 0:
    return ['spring-producer']
    #else:
    #    return repoList

def get_gradlebuild_list(path="."):
    # Get the list of all build.gradle in directory tree at given path
    fileList = list()
    for (dirpath, dirnames, filenames) in os.walk(path):
        fileList += [os.path.join(dirpath, file) for file in filenames if file=="build.gradle"]
    return fileList

def replace_proto_version(fileList,version):
    #Update protoversion in build.gradle

    for filePath in fileList:
        with open(filePath, 'r') as file :
            filedata = file.read()
        #print(filedata,end="\n\n\n")

        pattern = re.compile("com.cleartrip.analytics:protofiles:[0-9].[0-9].[0-9]_RELEASE")
        dependency_list = re.findall(pattern,str(filedata))
        #print(dependency_list,end="\n\n\n")
        if(len(dependency_list) != 0):
            old_version = dependency_list[0]
        else:
            print("No protofile dependency found in build.gradle")
            return

#         pattern = re.compile("'\s*com\.cleartrip\.analytics\s*'\s*,\s*name:\s*'protofiles'\s*,\s*version\s*:\s*'\s*[0-9]+\.[0-9]+\.[0-9]+_RELEASE\s*'")
#         dependency_list = re.findall(pattern,str(filedata))
#         if(len(dependency_list) != 0):
#             print(dependency_list)
#             repo.add(filePath)

#         pattern = re.compile("'\s*com\.cleartrip\.analytics\s*'\s*,\s*name:\s*'protofiles'\s*,\s*version\s*:\s*'\s*[0-9]+\.[0-9]+\.[0-9]+\s*'")
#         dependency_list = re.findall(pattern,str(filedata))
#         if(len(dependency_list) != 0):
#             print(dependency_list)
#             repo.add(filePath)

#         pattern = re.compile("com\.cleartrip\.analytics:protofiles:[0-9]+\.[0-9]+\.[0-9]+_RELEASE")
#         dependency_list = re.findall(pattern,str(filedata))
#         if(len(dependency_list) != 0):
#             print(dependency_list)
#             repo.add(filePath)

#         pattern = re.compile("com\.cleartrip\.analytics:protofiles:[0-9]+\.[0-9]+\.[0-9]+")
#         dependency_list = re.findall(pattern,str(filedata))
#         if(len(dependency_list) != 0):
#             print(dependency_list)
#             repo.add(filePath)


        new_version = "com.cleartrip.analytics:protofiles:" + version
        filedata = filedata.replace(old_version, new_version)
        print(filedata)
        with open(filePath, 'w') as file:
            file.write(filedata)

# For production

if len(sys.argv) == 1:
    print("Please specify version of protofile")
    os._exit(1)
elif len(sys.argv) == 2:
    new_version = str(sys.argv[1])
else:
    print("Usage : python <filname>.py <New version>")
    os._exit(1)

repo_list = getProtoUsingRepoList()
print("repo_list = " + repo_list[0])
for repo in repo_list:
    try:
        clone_url = "git clone https://manas_bhardwaj:RS2djuJr3DjYgf26LehY@bitbucket.org/manas_bhardwaj/" + repo + ".git"
        push_url = "git push https://manas_bhardwaj:RS2djuJr3DjYgf26LehY@bitbucket.org/manas_bhardwaj/" + repo + ".git"
        print("CLONING REPO : ",end="\n\n\n")
        os.system(clone_url)
        cd_path = "cd " + repo
        os.system(cd_path)
        print("Changing protofile version of repo : " + repo)
    except:
        print("Error occured while changing proto version in : " + repo)
