##### Start zookeeper-server
* `sudo bin/zkServer.sh start`

##### Start kafka-server 
* `sudo bin/kafka-server-start.sh config/server.properties`

##### Create a kafka-topic
* `sudo bin/kafka-topics.sh --create --zookeeper localhost:2181 --topic air-messages --partitions 1 --replication-factor 1`

##### Get List of kafka-topics
* `bin/kafka-topics.sh --list --zookeeper localhost:2181`

##### Start a kafka-producer
* ` bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test`

##### Start a kafka-consumer
* `bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --from-beginning`